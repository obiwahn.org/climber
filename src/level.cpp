// level.cpp

#include <iostream>
#include "level.h"
#include "levels.h"

bool Level::load(long nr) {
  if (nr < 1) {
    std::cerr << "Level number too small: " << nr << std::endl;
    return false;
  }
  size_t index = 0;
  while (--nr > 0) {
    ++index;
    if (levelData[index].start == nullptr) {
      std::cerr << "Level number too large: " << nr << " have only "
        << index << " levels." << std::endl;
      return false;
    }
  }
  LevelData& ld = levelData[index];
  std::string ls = std::string(reinterpret_cast<char*>(ld.start), ld.len);
  std::stringstream f(ls, std::ios::in);

  if (gravity != nullptr) {
    clear();
  }
  while (!f.eof()) {
    std::string line;
    getline(f, line);
    line.erase(std::remove(line.begin(), line.end(), '*'),
               line.end());  // remove *
    if (!line.empty()) {
      map.push_back(line);
      ++_rows;
    }
  }
  if (_rows == 0) {
    std::cerr << "Did not find any row, not loading." << std::endl;
    clear();
    return false;
  }
  if (_rows > 24) {
    std::cerr << "Cannot have more than 24 rows!" << std::endl;
    clear();
    return false;
  }
  // Sort out _cols:
  _cols = map[0].size();
  bool good = true;
  for (size_t i = 1; i < map.size(); ++i) {
    if (map[i].size() != _cols) {
      std::cerr << "Row " << i << " does not have the same length as row 0!"
        << std::endl;
      good = false;
    }
  }
  if (_cols > 32) {
    std::cerr << "Must not have more than 32 columns!" << std::endl;
    good = false;
  }
  if (!good) {
    clear();
    return false;
  }
  // Allocate gravity:
  gravity = new Direction[_rows * _cols];
  if (gravity == nullptr) {
    std::cerr << "Cannot allocate gravity array!" << std::endl;
    clear();
    return false;
  }
  // Compute gravity:
  std::deque<Pos> todo;
  for (size_t r = 0; r < _rows; ++r) {
    for (size_t c = 0; c < _cols; ++c) {
      char cc = map[r][c];
      switch (cc) {
        case 'V':
          gravity[r * _cols + c] = DirDown;
          todo.emplace_back(r, c);
          map[r][c] = ' ';
          break;
        case '^':
          gravity[r * _cols + c] = DirUp;
          todo.emplace_back(r, c);
          map[r][c] = ' ';
          break;
        case '<':
          gravity[r * _cols + c] = DirLeft;
          todo.emplace_back(r, c);
          map[r][c] = ' ';
          break;
        case '>':
          gravity[r * _cols + c] = DirRight;
          todo.emplace_back(r, c);
          map[r][c] = ' ';
          break;
        case 'S':
          gravity[r * _cols + c] = DirNone;
          map[r][c] = ' ';
          startRow = r;
          startCol = c;
          break;
        case '$':
          gravity[r * _cols + c] = DirNone;
          map[r][c] = ' ';
          _golds.emplace_back(r, c);
          break;
        case 'D':
          gravity[r * _cols + c] = DirNone;
          break;
        case 'M':
          gravity[r * _cols + c] = DirNone;
          map[r][c] = ' ';
          monsters.emplace_back(r, c);
          break;
        default:
          gravity[r * _cols + c] = DirNone;
          break;
      }
    }
  }
  while (!todo.empty()) {
    Pos p = todo.front();
    todo.pop_front();
    Direction d = gravity[p.r * _cols + p.c];
    // Now copy to all neighbours which are not yet set:
    std::vector<Pos> neighbours;
    if (p.r > 0) { neighbours.emplace_back(p.r-1, p.c); }
    if (p.r < _rows-1) { neighbours.emplace_back(p.r+1, p.c); }
    if (p.c > 0) { neighbours.emplace_back(p.r, p.c-1); }
    if (p.c < _cols-1) { neighbours.emplace_back(p.r, p.c+1); }
    for (auto const& pp : neighbours) {
      if (gravity[pp.r * _cols + pp.c] == DirNone) {
        gravity[pp.r * _cols + pp.c] = d;
        todo.push_back(pp);
      }
    }
  }
  return true;
}

void Level::show() {
  std::cout << "Rows: " << _rows << " Cols: " << _cols << std::endl;
  for (size_t r = 0; r < _rows; ++r) {
    std::string line;
    for (size_t c = 0; c < _cols; ++c) {
      switch (gravity[r * _cols + c]) {
        case DirUp:    line.push_back('^'); break;
        case DirDown:  line.push_back('V'); break;
        case DirLeft:  line.push_back('<'); break;
        case DirRight: line.push_back('>'); break;
        default:               line.push_back(' '); break;
      }
    }
    std::cout << line << std::endl;
  }
}

std::string Level::mapLetters(SDL_Rect& obj, char stopIf) {
  char buf[128];
  memset(buf, 0, 128);
  std::string result;
  int xtile = obj.x / TILE_WIDTH;
  int ytile = obj.y / TILE_HEIGHT;
  int xtileright = (obj.x + obj.w - 1) / TILE_WIDTH;
  int ytilebottom = (obj.y + obj.h - 1) / TILE_HEIGHT;
  for (int i = xtile; i <= xtileright; ++i) {
    for (int j = ytile; j <= ytilebottom; ++j) {
      char c = map[j][i];
      if (buf[c] == 0) {
        result.push_back(c);
        buf[c] = 1;
      }
      if (c == stopIf) {
        return result;
      }
    }
  }
  return result;
}

Level::Direction Level::feelsGravity(SDL_Rect& obj, int& dx, int& dy) {
  // Returns a direction if the center of the object is on a tile with
  // gravity in that direction, or DirNone, if there is no gravity
  // here. If the object can move one pixel in this direction, then dx and
  // dy are set accordingly, otherwise they are set to 0. This also happens
  // if there is no gravity. Note that obj must be within the screen!
  int x = obj.x + obj.w/2;
  int y = obj.y + obj.h/2;
  int xtile = x / TILE_WIDTH;
  int ytile = y / TILE_HEIGHT;
  if (map[ytile][xtile] == '#') {
    dx = 0;
    dy = 0;
    return DirNone;
  }
  Direction d = gravity[ytile * _cols + xtile];
  switch (d) {
    case DirNone:
      dx = 0; dy = 0;
      return d;
    case DirUp:
      dx = 0; dy = -1;
      break;
    case DirDown:
      dx = 0; dy = 1;
      break;
    case DirLeft:
      dx = -1; dy = 0;
      break;
    case DirRight:
      dx = 1; dy = 0;
      break;
  }

  std::string found = mapLetters(obj, '#');
  if (found.find('#') != std::string::npos) {
    dx = 0; dy = 0;
    return DirNone;
  }

  SDL_Rect moved = {obj.x + dx, obj.y + dy, obj.w, obj.h};
  if (!isInMap(moved)) {
    dx = 0; dy = 0;
    return d;
  }
  found = mapLetters(moved, 'X');
  if (found.find('X') == std::string::npos &&
      found.find('#') == std::string::npos) {
    return d;
  }
  dx = 0; dy = 0;
  if (found.find('X') != std::string::npos) {
    // Solid ground, do not allow movement in the direction of gravity.
    return d;
  } else {
    // Only a ladder prevents gravity, so allow any movement.
    return DirNone;
  }
}

std::array<Level::Pos, 5> const Level::neighbors
  = {Pos(-1, 0), Pos(0, 1), Pos(1, 0), Pos(0, -1), Pos(0, 0)};

void Level::computeDirToPlayer(int r, int c, bool force) {
  if (r == lastr && c == lastc && !force) {
    return;
  }
  lastr = r;
  lastc = c;
  if (dirToPlayer.empty()) {
    for (size_t i = 0; i < _rows; ++i) {
      dirToPlayer.emplace_back(_cols, int(DirUndiscovered));
    }
  } else {
    for (size_t i = 0; i < _rows; ++i) {
      for (size_t j = 0; j < _cols; ++j) {
        dirToPlayer[i][j] = DirUndiscovered;
      }
    }
  }

  std::deque<Pos> todo {Pos(r, c)};
  dirToPlayer[r][c] = DirNone;   // Mark as discovered
  while (!todo.empty()) {
    auto const& pos = todo.front();
    for (Direction d = DirUp; d <= DirLeft; ++d) {
      int rr = pos.r + neighbors[d].r;
      int cc = pos.c + neighbors[d].c;
      if (rr >= 0 && cc >= 0 && rr < _rows && cc < _cols &&
          map[rr][cc] != 'X' && dirToPlayer[rr][cc] == DirUndiscovered) {
        // Can one go from (rr, cc) to (r, c)?
        bool reachable = false;
        if (map[rr][cc] == '#') {
          reachable = true;
          // otherwise: either empty or a door, which means the same
        } else {
          Direction g = gravity[rr * _cols + cc];
          if ((g ^ 2) == d) {
            // gravity points that way, so we can fall
            reachable = true;
          } else if (g == d) {
            // gravity points the other way, so we cannot reach
            reachable = false;
          } else {
            // gravity points sideways, we can only reach the original point
            // if there is something to stand on in the direction of gravity:
            int rrr = rr + neighbors[g].r;
            int ccc = cc + neighbors[g].c;
            reachable = (rrr < 0 || rrr >= _rows || ccc < 0 | ccc >= _cols ||
                         map[rrr][ccc] == '#' || map[rrr][ccc] == 'X');
          }
        }
        if (reachable) {
          todo.emplace_back(rr, cc);
          dirToPlayer[rr][cc] = d ^ 2;
        }
      }
    }
    todo.pop_front();
  }
#if 0
  for (size_t i = 0; i < _rows; ++i) {
    for (size_t j = 0; j < _cols; ++j) {
      switch(dirToPlayer[i][j]) {
        case DirUp: std::cout << '^'; break;
        case DirDown: std::cout << 'V'; break;
        case DirLeft: std::cout << '<'; break;
        case DirRight: std::cout << '>'; break;
        case DirNone: std::cout << ' '; break;
        case DirUndiscovered: std::cout << 'X'; break;
      }
    }
    std::cout << "\n";
  }
#endif
}

