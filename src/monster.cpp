// monster.cpp

#include "graphics.h"
#include "level.h"
#include "monster.h"

void Monster::move(Level& l) {
  if (sleeping) {
    if (std::chrono::steady_clock::now() <= sleepingUntil) {
      return;
    }
    sleeping = false;
  }

  // Compute speed of monster:
  // First find out if it falls:
  int dx;
  int dy;
  Level::Direction d = l.feelsGravity(pos, dx, dy);

  if (d != Level::DirNone && (dx != 0 || dy != 0)) {
    vx = speed * dx;
    vy = speed * dy;
  } else {
    // Determine wanted direction by dirToPlayer graph:
    int tilec = getTileCol();
    int tiler = getTileRow();
    Level::Direction dd = l.dirToPlayer[tiler][tilec];
    want_vx = Level::neighbors[dd].c;
    want_vy = Level::neighbors[dd].r;

    int pdx = want_vx;
    int pdy = want_vy;
    if (d == Level::DirUp || d == Level::DirDown) {
      // Vertical gravity, monster can only move horizontally:
      pdy = 0;
      if (pdx == 0) {   // want vertical, so move to center of tile:
        pdx = (pos.x + (pos.w/2) < tilec * TILE_WIDTH + TILE_WIDTH/2)
              ? 1 : -1;
      }
    } else if (d == Level::DirLeft || d == Level::DirRight) {
      // Horizontal gravity, player can only move vertically:
      pdx = 0;
      if (pdy == 0) {   // want horizontal, so move to center of tile:
        pdy = (pos.y + (pos.h/2) < tiler * TILE_HEIGHT + TILE_HEIGHT/2)
              ? 1 : -1;
      }
    }
    bool triedOrthogonal = false;
    while (pdx != 0 || pdy != 0) {
      SDL_Rect newPos = pos;
      newPos.x += pdx;
      newPos.y += pdy;
      if (l.positionPossible(newPos)) {
        break;
      }
      if (triedOrthogonal) {
        pdx = 0;
        pdy = 0;
      } else {
        triedOrthogonal = true;
        // We could not move as wanted, let's move in the orthogonal
        // direction towards the middle:
        if (dd == Level::DirUp || dd == Level::DirDown) {
          pdy = 0;
          pdx = (pos.x + (pos.w/2) < tilec * TILE_WIDTH + TILE_WIDTH/2)
                ? 1 : -1;
        } else {
          pdx = 0;
          pdy = (pos.y + (pos.h/2) < tiler * TILE_HEIGHT + TILE_HEIGHT/2)
                ? 1 : -1;
        }
      }
    }
    vx = speed * pdx;
    vy = speed * pdy;
  }

  x += vx;
  y += vy;
  pos.x = static_cast<int>(floor(x + 0.5));
  pos.y = static_cast<int>(floor(y + 0.5));
}

