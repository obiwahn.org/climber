// graphics.h - global graphics definitions
#pragma once
#ifndef CLIMBER_GRAPHICS_H
#define CLIMBER_GRAPHICS_H 1

#include <vector>
#include <memory>
#include <SDL.h>
//#include <SDL_image.h>

constexpr int SCREEN_WIDTH = 1024;
constexpr int SCREEN_HEIGHT = 768;

constexpr int TILE_WIDTH = 32;
constexpr int TILE_HEIGHT = 32;

constexpr int PLAYER_WIDTH = 21;
constexpr int PLAYER_HEIGHT = 21;
constexpr int PLAYER_HALF = 11;

extern SDL_Window* gWindow;
extern SDL_Renderer* gRenderer;

bool checkCollision(SDL_Rect a, SDL_Rect b);

class Tile;
extern std::vector<std::unique_ptr<Tile>> tiles;
#endif
