// media.h

#ifndef MEDIA_H
#define MEDIA_H 1

#include <vector>
#include <memory>

#include "SDL2/SDL.h"
#include "SDL2/SDL_mixer.h"

#include "tile.h"
#include "level.h"

class Media {
 public:
  Media() 
    : _background(nullptr), _nrJoy(-1) {}
  ~Media();
  // Initializes SDL, returns true if OK and false if error:
  bool init(bool fullscreen); 
  bool loadMedia();  // Loads media from disk
  bool loadMediaFromMemory();   // Loads media from executable
  Tile* getTile(size_t nr) {
    return _tiles[nr].get();
  }
  Tile* getShape(size_t nr) {
    return _shapes[nr].get();
  }
  Mix_Chunk* getSound(size_t nr) {
    return _sounds[nr];
  }
  void makeBackground(Level const& l);
  SDL_Texture* getBackground() const {
    return _background;
  }

  bool haveJoystick() {
    return _nrJoy > 0;
  }

  static size_t constexpr TileUp = 0;
  static size_t constexpr TileRight = 1;
  static size_t constexpr TileDown = 2;
  static size_t constexpr TileLeft = 3;
  static size_t constexpr TileEmpty = 4;
  static size_t constexpr TileBlock = 5;
  static size_t constexpr TileLadder = 6;
  static size_t constexpr TileDoor = 7;
  static size_t constexpr ShapePlayer = 0;
  static size_t constexpr ShapeGold = 1;
  static size_t constexpr ShapeBomb = 2;
  static size_t constexpr ShapeExplosion = 3;
  static size_t constexpr ShapeMonster = 4;
  static size_t constexpr ShapeMonster2 = 5;
  static size_t constexpr ShapeMonster3 = 6;
  static size_t constexpr ShapeGameOver = 7;
  static size_t constexpr SoundPling = 0;
  static size_t constexpr SoundExplosion = 1;

 private:
  // The tiles:
  std::vector<std::unique_ptr<Tile>> _tiles;
  // The moving shapes:
  std::vector<std::unique_ptr<Tile>> _shapes;
  // The sounds:
  std::vector<Mix_Chunk*> _sounds;
  SDL_Texture* _background;
  // Joysticks?
  int _nrJoy;
};

#endif
