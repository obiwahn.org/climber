// tile.h
#ifndef CLIMBER_TILE_H
#define CLIMBER_TILE_H 1

#include "graphics.h"
#include <iostream>
#include <cassert>

// We assume that "graphics.h" has already been included (since it includes us).

class Tile {
 public:
  Tile() : _surface(nullptr), _texture(nullptr), _width(TILE_WIDTH), _height(TILE_HEIGHT) {
  }

  ~Tile() {
    free();
  }
 private:

  bool finishRead(SDL_Surface* loadedSurface, SDL_Window* window) {
    assert(window != nullptr);
    if (loadedSurface == nullptr) {
      std::cerr << "Unable to load image {}! SDL_image Error: "
                << SDL_GetError() << std::endl;
      return false;
    }

    SDL_SetColorKey(loadedSurface, SDL_TRUE,
        SDL_MapRGB(loadedSurface->format, 0, 0xff, 0xff));

    auto screen = SDL_GetWindowSurface(window);
    if(screen == nullptr) {
      std::cerr << "Unable to get screen ! SDL Error: "
                << SDL_GetError() << std::endl;
      return false;
    }
    _surface = SDL_ConvertSurface(loadedSurface, screen->format, 0);

    SDL_Texture* newTexture = nullptr;
    newTexture = SDL_CreateTextureFromSurface(gRenderer, _surface);

    if (newTexture == nullptr) {
      std::cerr << "Unable to create texture! SDL Error: "
                << SDL_GetError() << std::endl;
      SDL_FreeSurface(loadedSurface);
      return false;
    }

    _width = _surface->w;
    _height = _surface->h;
    SDL_FreeSurface(loadedSurface);
    _texture = newTexture;
    return true;
  }

 public:

  bool loadFromFile(std::string const& path, SDL_Window* window) {
    free();
    return finishRead(SDL_LoadBMP(path.c_str()), window);
  }

  bool loadFromMemory(unsigned char* start, unsigned int len, SDL_Window* window) {
    free();
    SDL_RWops* rwops = SDL_RWFromConstMem(start, static_cast<int>(len));
    return finishRead(SDL_LoadBMP_RW(rwops, 1), window);
  }

#ifdef _SDL_TTF_H
  bool loadFromRenderedText(std::string const& textureText, SDL_Color textColor) {
    free();

    SDL_Surface* textSurface
      = TTF_RenderText_Solid(gFont, textureText.c_str(), textColor);
    if (textSurface == nullptr) {
      std::cerr << "Unable to render text surface! SDL_ttf Error: "
                << TTF_GetError() << std::endl;
      return false;
    }
    _texture = SDL_CreateTextureFromSurface( gRenderer, textSurface);
    if (_texture == nullptr) {
      std::cerr << "Unable to create texture from rendered text! SDL Error: "
        << SDL_GetError() << std::endl;
      SDL_FreeSurface(textSurface);
      return false;
    }
    _width = textSurface->w;
    _height = textSurface->h;
    SDL_FreeSurface(textSurface);
    return true;
  }
#endif

  void free() {
    if (_texture != nullptr) {
      SDL_FreeSurface(_surface);
      SDL_DestroyTexture(_texture);
      _surface = nullptr;
      _texture = nullptr;
      _width = 0;
      _height = 0;
    }
  }

  void setColor(uint8_t red, uint8_t green, uint8_t blue) {
    SDL_SetTextureColorMod(_texture, red, green, blue);
  }

  void setBlendMode(SDL_BlendMode blending) {
    SDL_SetTextureBlendMode(_texture, blending);
  }

  void setAlpha(uint8_t alpha) {
    SDL_SetTextureAlphaMod(_texture, alpha);
  }

  void render(SDL_Rect& camera, int x, int y) {
    SDL_Rect renderQuad = {x, y, _width, _height};
    if (checkCollision(camera, renderQuad)) {
      renderQuad.x = renderQuad.x - camera.x;
      renderQuad.y = renderQuad.y - camera.y;
      SDL_RenderCopy(gRenderer, _texture, nullptr, &renderQuad);
    }
  }

  void blit(int x, int y, SDL_Surface* dest) {
    SDL_Rect renderQuad = {x, y, _width, _height};
    SDL_BlitSurface(_surface, nullptr, dest, &renderQuad);
  }

  int getWidth() const {
    return _width;
  }

  int getHeight() const {
    return _height;
  }

 private:
  SDL_Surface* _surface;
  SDL_Texture* _texture;
  int _width;
  int _height;
};

#endif
